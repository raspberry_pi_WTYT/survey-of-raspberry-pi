from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase

from AppSurvey.models import Note
from AppSurvey.views import home_page, note_detail, note_edit, search_result
import unittest
import pep8
from django.utils import timezone


class TestCodeFormat(unittest.TestCase):

    def test_pep8_conformance(self):  # check pep8
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('A2/views.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) views.py" % file_errors)
        fchecker = pep8.Checker('A2/models.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) models.py" % file_errors)
        fchecker = pep8.Checker('superlists/urls.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) urls.py" % file_errors)
        fchecker = pep8.Checker('functional_tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) functional_tests.py"
              % file_errors)
        fchecker = pep8.Checker('A2/tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) tests.py" % file_errors)


class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)


class TestSave(TestCase):

    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['title_text'] = 'Hello Google'
        request.POST['url_text'] = 'http://www.google.com'
        request.POST['note_text'] = 'very good search engine'
        request.POST['add_form'] = 'Submit'

        response = home_page(request)

        self.assertEqual(Note.objects.count(), 1)
        new_title = Note.objects.first()
        self.assertEqual(new_title.title, 'Hello Google')
        self.assertEqual(new_title.url, 'http://www.google.com')
        self.assertEqual(new_title.message, 'very good search engine')

    def test_home_page_redirects_after_POST(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['title_text'] = 'Hello Google'
        request.POST['url_text'] = 'http://www.google.com'
        request.POST['note_text'] = 'very good search engine'
        request.POST['add_form'] = 'Submit'

        response = home_page(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Note.objects.count(), 0)


class ListViewTest(TestCase):

    def test_home_page_displays_all_list_items(self):
        Note.objects.create(title='Hello Google', pub_date=timezone.now())

        request = HttpRequest()
        response = home_page(request)

        self.assertIn('Hello Google', response.content.decode())


class NoteModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        first_text = Note()
        first_text.title = 'Hello'
        first_text.url = 'www.google.com'
        first_text.message = 'test'
        first_text.pub_date = timezone.now()
        first_text.save()

        saved_text = Note.objects.all()
        self.assertEqual(saved_text.count(), 1)

        first_saved_text = saved_text[0]
        self.assertEqual(first_saved_text.title, 'Hello')
        self.assertEqual(first_saved_text.url, 'www.google.com')
        self.assertEqual(first_saved_text.message, 'test')

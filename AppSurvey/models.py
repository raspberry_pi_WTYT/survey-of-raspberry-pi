import datetime

from django.db import models
from django.utils import timezone


class Survey(models.Model):
    question = models.TextField()
    answer = models.IntegerField()
    # pub_date = models.DateTimeField(timezone.now())

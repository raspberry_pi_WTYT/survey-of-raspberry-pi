from django.shortcuts import redirect, render
from AppSurvey.models import Survey
from django.forms import ModelForm


question = ["test"+str(i+1) for i in range(20)]


class Point:
    def __init__(self, question, score, number):
        self.question = question
        self.score = score
        self.number = number


def home_page(request):
    if request.method == 'POST':
        for i in range(len(question)):
            Survey.objects.create(question=question[i],
                                  answer=int(request.POST[question[i]]))
        return redirect('/ShowResult/')
    return render(request, 'home.html', {'questions': question})


def ShowResult(request):
    sendValue = []
    for i in range(len(question)):
        surveys = Survey.objects.filter(question=question[i])
        avg = 0.0
        for j in surveys:
            avg = avg + j.answer
        if len(surveys) != 0:
            avg = avg / len(surveys)
        sendValue.append(Point(question[i], avg, len(surveys)))
    return render(request, 'show.html', {'sendValue': sendValue})
